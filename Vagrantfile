# -*- mode: ruby -*-
# vi: set ft=ruby :

#
# Read config from YAML file
#
require 'yaml'
settings = YAML.load_file('setting-config.yml')



#TODO How to remove adding double defining IP for nfsclients
# Issue is that host nfs is provisioned first, then nfs restarted - at that point
# /etc/hosts doesn't contain the IPs.
#   maybe we can just share to the network?
#   or use variables for hostname => ip
#

Vagrant.configure("2") do |config|

  # Manually update the Vagrant boxes - vagrant box update
  config.vbguest.auto_update = false

  #TODO - there is hard-coding usage of 18.04 Bionic, see gl-runner-docker.sh
  config.vm.box = "bento/ubuntu-18.04"
  #TODO Check that directories config.vm.box/apt /apt_lists exists, if not then create them
  #  check if we are on OSx
  #  mkdir /Users/astrachan/.vagrant.d/cache/bento/ubuntu-18.04/apt
  #  mkdir /Users/astrachan/.vagrant.d/cache/bento/ubuntu-18.04/apt_lists

  #TODO link to hostsupdater plugin
  config.hostsupdater.remove_on_suspend = false

  #
  # Multi-host setup
  #


  #error - fixed by setting the synced_folder_opts options
  # ==> nfs: Can't drop privileges for downloading as file '/var/lib/apt/lists/partial/archive.ubuntu.com_ubuntu_dists_xenial_InRelease' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
  if Vagrant.has_plugin?("vagrant-cachier")
    # Configure cached packages to be shared between instances of the same base box.
    # More info on http://fgrehm.viewdocs.io/vagrant-cachier/usage
    config.cache.scope = :box

    config.cache.synced_folder_opts = {
        owner: "_apt",
        group: "_apt",
        #mount_options: ["dmode=777", "fmode=666"]
    }
  end


  #
  # Host: nfs
  #
  config.vm.define settings['roles']['nfs_role']['name'] do |nfs_node|
    nfs_node.vm.boot_timeout = 600
    nfs_node.vm.hostname = settings['roles']['nfs_role']['name_fqdn']

    nfs_node.vm.network :private_network, :ip => settings['roles']['nfs_role']['ip']
    nfs_node.vm.provision :hosts do |provisioner|
      provisioner.autoconfigure = true
      provisioner.sync_hosts = true
    end

    nfs_node.vm.provider "virtualbox" do |vb|
      vb.gui    = settings['roles']['nfs_role']['gui']
      vb.cpus   = settings['roles']['nfs_role']['cpus']
      vb.memory = settings['roles']['nfs_role']['memory']
    end

    nfs_node.vm.provision "shell", name: "provision/sh/bootstrap.sh on vm #{settings['roles']['nfs_role']['name']} Bootstrap", path: "provision/sh/bootstrap.sh", args: settings['setup']['timezone']
    nfs_node.vm.provision "shell", name: "provision/sh/timesync-checkpoint.sh on vm #{settings['roles']['nfs_role']['name']} Timesync Checkpoint", path: "provision/sh/timesync-checkpoint.sh"
    nfs_node.vm.provision "shell", name: "provision/sh/tools.sh on vm #{settings['roles']['nfs_role']['name']} Tools", path: "provision/sh/tools.sh"
    nfs_node.vm.provision "shell", name: "provision/sh/nfs-server.sh on vm #{settings['roles']['nfs_role']['name']} Configure NFS Server", path: "provision/sh/nfs-server.sh"
  end



  #
  # Host: app_node
  #
  settings['roles']['app_role'].each do |app_node|
    next unless app_node['enabled']
    config.vm.define app_node['name'] do |node|
      node.vm.boot_timeout = 600
      node.vm.hostname = app_node['name_fqdn']
      node.vm.network :private_network, :ip => app_node['ip']
      node.vm.provision :hosts, :sync_hosts => true

      node.vm.provider "virtualbox" do |vb|
        vb.gui    = app_node['gui']
        vb.cpus   = app_node['cpus']
        vb.memory = app_node['memory']
      end

      node.vm.provision "shell", name: "provision/sh/bootstrap.sh on vm #{app_node['name']} Bootstrap", path: "provision/sh/bootstrap.sh", args: settings['setup']['timezone']
      node.vm.provision "shell", name: "provision/sh/timesync-checkpoint.sh on vm #{app_node['name']} Timesync Checkpoint", path: "provision/sh/timesync-checkpoint.sh"
      node.vm.provision "shell", name: "provision/sh/tools.sh on vm #{app_node['name']} Tools", path: "provision/sh/tools.sh"
      node.vm.provision "shell", name: "provision/sh/nfs-client.sh on vm #{app_node['name']} NFS Client", path: "provision/sh/nfs-client.sh",
                        args: [settings['setup']['nfs'],app_node['name']]
      node.vm.provision "shell", name: "provision/sh/gl-bootstrap.sh on vm #{app_node['name']} Install GitLab #{settings['setup']['gitlab-version']}", path: "provision/sh/gl-bootstrap.sh", args: settings['setup']['gitlab-version']
      node.vm.provision "shell", name: "provision/sh/gl-set-external-url.sh on vm #{app_node['name']} Set external_url", path: "provision/sh/gl-set-external-url.sh",
                            args: settings['setup']['external_url']
      node.vm.provision "shell", name: "provision/sh/gl-set-nfs.sh on vm #{app_node['name']} Configure NFS", path: "provision/sh/gl-set-nfs.sh",
                            args: settings['setup']['nfs']
      node.vm.provision "shell", name: "provision/sh/gl-set-app-config.sh on vm #{app_node['name']} Configure Application ", path: "provision/sh/gl-set-app-config.sh"
      node.vm.provision "shell", name: "provision/sh/gl-reconfigure.sh on vm #{app_node['name']} Reconfigure", path: "provision/sh/gl-reconfigure.sh"
      node.vm.provision "shell", name: "provision/sh/gl-copy-secrets-to-store.sh on vm #{app_node['name']} Store Secrets", path: "provision/sh/gl-copy-secrets-to-store.sh"
    end
  end

  #
  # Host: gitaly_node
  #
  settings['roles']['gitaly_role'].each do |gitaly_node|
    next unless gitaly_node['enabled']
    config.vm.define gitaly_node['name'] do |node|
      node.vm.boot_timeout = 600
      node.vm.hostname = gitaly_node['name_fqdn']
      node.vm.network :private_network, :ip => gitaly_node['ip']
      node.vm.provision :hosts, :sync_hosts => true

      node.vm.provider "virtualbox" do |vb|
        vb.gui = gitaly_node['gui']
        vb.cpus = gitaly_node['cpus']
        vb.memory = gitaly_node['memory']
      end

      node.vm.provision "shell", name: "provision/sh/bootstrap.sh on vm #{gitaly_node['name']} Bootstrap", path: "provision/sh/bootstrap.sh", args: settings['setup']['timezone']
      node.vm.provision "shell", name: "provision/sh/timesync-checkpoint.sh on vm #{gitaly_node['name']} Timesync Checkpoint", path: "provision/sh/timesync-checkpoint.sh"
      node.vm.provision "shell", name: "provision/sh/tools.sh on vm #{gitaly_node['name']} Tools", path: "provision/sh/tools.sh"
      node.vm.provision "shell", name: "provision/sh/nfs-client.sh on vm #{gitaly_node['name']} NFS Client", path: "provision/sh/nfs-client.sh",
                        args: [settings['setup']['nfs'],gitaly_node['name']]
      node.vm.provision "shell", name: "provision/sh/gl-bootstrap.sh on vm #{gitaly_node['name']} Install GitLab #{settings['setup']['gitlab-version']}", path: "provision/sh/gl-bootstrap.sh", args: settings['setup']['gitlab-version']
      node.vm.provision "shell", name: "provision/sh/gl-set-external-url.sh on vm #{gitaly_node['name']} Set external_url", path: "provision/sh/gl-set-external-url.sh", args: settings['setup']['external_url']
      node.vm.provision "shell", name: "provision/sh/gl-gitaly-config-pre-reconfigure.sh on vm #{gitaly_node['name']} Pre-Reconfigure gitaly", path: "provision/sh/gl-gitaly-config-pre-reconfigure.sh", args: gitaly_node['name']
      node.vm.provision "shell", name: "provision/sh/gl-copy-secrets-from-store.sh on vm #{gitaly_node['name']} Copy Secrets", path: "provision/sh/gl-copy-secrets-from-store.sh"
      node.vm.provision "shell", name: "provision/sh/gl-reconfigure.sh on vm #{gitaly_node['name']} Reconfigure", path: "provision/sh/gl-reconfigure.sh"
      node.vm.provision "shell", name: "provision/sh/gl-gitaly-config-pos-reconfigure.sh on vm #{gitaly_node['name']} Pos-Reconfigure gitaly", path: "provision/sh/gl-gitaly-config-pos-reconfigure.sh", args: gitaly_node['name']
      node.vm.provision "shell", name: "provision/sh/gl-gitaly-checkpoint.sh on vm #{gitaly_node['name']} Checkpoint", path: "provision/sh/gl-gitaly-checkpoint.sh", run: "always"
    end
  end

  #
  # Host: runner_node
  #
  settings['roles']['runner_role'].each do |runner_node|
    next unless runner_node['enabled']
    config.vm.define runner_node['name'] do |node|
      node.vm.boot_timeout = 600
      node.vm.hostname = runner_node['name_fqdn']
      node.vm.network :private_network, :ip => runner_node['ip']
      node.vm.provision :hosts, :sync_hosts => true

      node.vm.provider "virtualbox" do |vb|
        vb.gui = runner_node['gui']
        vb.cpus = runner_node['cpus']
        vb.memory = runner_node['memory']
      end

      node.vm.provision "shell", name: "provision/sh/bootstrap.sh on vm #{runner_node['name']} Bootstrap", path: "provision/sh/bootstrap.sh", args: settings['setup']['timezone']
      node.vm.provision "shell", name: "provision/sh/timesync-checkpoint.sh on vm #{runner_node['name']} Timesync Checkpoint", path: "provision/sh/timesync-checkpoint.sh"
      node.vm.provision "shell", name: "provision/sh/tools.sh on vm #{runner_node['name']} Tools", path: "provision/sh/tools.sh"
      node.vm.provision "shell", name: "provision/sh/gl-runner-bootstrap.sh on vm #{runner_node['name']} Install GitLab #{settings['setup']['gitlab-runner-version']}", path: "provision/sh/gl-runner-bootstrap.sh", args: settings['setup']['gitlab-runner-version']
      node.vm.provision "shell", name: "provision/sh/gl-runner-docker.sh on vm #{runner_node['name']} Install Docker", path: "provision/sh/gl-runner-docker.sh", args: settings['setup']['external_url']
      node.vm.provision "shell", name: "provision/sh/gl-runner-register.sh on vm #{runner_node['name']} Register Runner", path: "provision/sh/gl-runner-register.sh", args: [settings['setup']['external_url'],settings['setup']['external_ip']]
      node.vm.provision "shell", name: "on vm #{runner_node['name']} Checkpoint", path: "provision/sh/gl-runner-checkpoint.sh", run: "always"
    end
  end  
  
end
