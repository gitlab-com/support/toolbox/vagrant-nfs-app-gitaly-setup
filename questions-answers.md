# Questions and Answers  (As at Aug 2019)

## Do the projects on the Gitaly nodes get backed up?
### Backup
Ensure multiple Gitaly nodes are configured
Ensure `/admin/application_settings/repository` > Repository storage is **Checked** for multiple storage paths
Create 3 Projects

1. Project100
1. Project101
1. Project102

Confirm the location of projects on the filesystem

```shell script
### ls listing from the host
```
Run backup

```shell script
sudo gitlab-rake gitlab:backup:create
```

Delete project103

### Restore - Will restore put the projects back to the original nodes?
Run restore

```shell script
sudo gitlab-rake gitlab:backup:restore
```

Confirm the location of projects on the filesystem

```shell script
# show renamed directory
# show Project102
```

## Should I update Gitaly nodes before App node?
Show `admin/gitaly_servers`, highlight **green** traffic light.  Only go! when nodes are on the same version.
Use [Omnibus Multi Node](https://docs.gitlab.com/omnibus/update/#multi-node--ha-deployment)

## How do I move projects around Gitaly nodes?


## How to recover a single Gitaly node?


