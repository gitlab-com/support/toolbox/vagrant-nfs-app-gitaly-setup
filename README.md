# Multi Host GitLab Installation Focusing on Gitaly Configurations
 
## Introduction
This project will stand up a local **workshop/testing** environment for exploring a multi-host GitLab setup focused on **Gitaly**.  It allows installation of different versions by using `git tags` to set the git working directory to a known good state for that version.

It provides a sandpit for [Operational Tasks](#operational-tasks) and support bootcamps.

You can also explore using the latest GitLab by updating the [setting-config.yml](setting-config.ym) file.

Using `vagrant snapshot` gives easy rollback ability when testing upgrades or destructive operations

See [Introduction Video - TBD]()
 
### Provisions 
VMs Provisioned (default settings)
* `nfs` NFS backend storage - 2 CPU, 512MB
* `app` GitLab App - 2 CPU, 3072MB
* `gitaly0[12]` GitLab Gitaly Standalone - 2 CPU, 1024MB
* `runner-1084[12]` GitLab Runners - 2 CPU, 512MB

### Preconfigured
* Runners - Shared shell and docker executors accepting all jobs
* Projects Storage both local and NFS available on `app`, `gitaly0[12]`

Allows testing of Gitaly storage options and CICD.

![Gitaly Servers - app, gitaly01 and gitaly02](Admin_Area_Gitaly_Servers.png)
![Repostiory Storage](Admin_Area_Repostiory_Storage.png)

### Secrets

This directory is being used to store any file containing secrets as needed by the provisioning process
e.g.
```shell script
gitlab-secrets.json
```

The directory has been added to `.gitignore` so that it's only on your local storage.



## Getting Started

See [Setup](setup.md) to install dependencies.

1. Clone the project

2. Review settings

Check file `setting-config.yml`

* Timezone - see [complete-timezone-list](https://devanswers.co/ubuntu-change-timezone-synchronize-ntp/#complete-timezone-list)
  - Australia/Brisbane
  - America/Los_Angeles
* Version

3. Initialise Vagrant VMs (20-30 mins)

```
time vagrant up   ### Takes around 20 - 30mins
```

* login to http://app.vagrant

4. Enable Gitaly storage in UI

Go to Admin > Settings > Repository > Repository Storage and include
* Storage1
* Storage2

5. Snapshot install to allow rollback ability! 

```shell script
vagrant snapshot save gitlab-ee-install-11-11-5
==> nfs: Snapshotting the machine as 'gitlab-ee-install-11-11-5'...
==> nfs: Snapshot saved! You can restore the snapshot at any time by
==> nfs: using `vagrant snapshot restore`. You can delete it using
==> nfs: `vagrant snapshot delete`.
==> app: Snapshotting the machine as 'gitlab-ee-install-11-11-5'...
==> gitaly01: Snapshotting the machine as 'gitlab-ee-install-11-11-5'...
...
```

## Install a specific GitLab version
We are using `git tags` [Git-Basics-Tagging](https://git-scm.com/book/en/v2/Git-Basics-Tagging), when a successful setup installation has been completed - we `git tag -a vX.Y.Z` and push that back to the project.

To allow us to provision a specific version while continuing to keep pace with any changes on how this setup should be installed as GitLab iterates.  This will mean that we don't need to add logic in the setup for dealing with different versions.

The scope of this project is to provision a simple base setup to allow further manual configuration for testing.

* Clone the project (use a different directory for multiple versions of GitLab)
* List available tags `git tag`
* Checkout a specific tag `git checkout tag`

To develop on the project, don't use tags and follow the GitLab Flow.

1. Create an issue
1. Create a merge request
1. Checkout the branch
1. `vagrant destroy -f` which will delete any existing VMs and snapshots
1. Update GitLab version and gitlab-runner **version** in `settings-config.yml`
1. On the project page in GitLab.com add a tag against your commit

## <a name="operational-tasks"></a>Operational Tasks

Wanted to verify questions from customers like
* Do the projects on the Gitaly nodes get backed up? [backup-restore](questions-answers/backup-restore/backup-restore.md)
* Will `restore` put the projects back to the original nodes? [backup-restore](questions-answers/backup-restore/backup-restore.md)
* Should I update Gitaly nodes before App node? [update-software](questions-answers/update-software/update-software.md)
* How do I move projects around Gitaly nodes?
* How to recover a single Gitaly node?
* Enable TLS on a Gitaly node?

Gitaly for operational use is still young with many of the above questions not being able to be done easily yet, this allows a **sandpit** for testing these.

* [Explore Gitaly Operational Tasks](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1809)

## Common **Vagrant** commands

```
vagrant up                                     # Provisions all of the hosts defined in the Vagranrt file
vagrant up nfs                                 # Provisions thde VM **nfs**
vagrant snapshot save 01-installed-xx.yy.xx    # Creates a snapshots of all hosts 
vagrant destroy -f nfs                         # Delete the VM **nfs**
vagrant destroy -f                             # Deletes all of the VMs and snapshots
```

## Vagrant plugins

### vagrant-hostsupdater

Hostnames are automatically added to your local `/etc/hosts`

```text
10.8.0.5  nfs.vagrant          # NFS Storage
10.8.0.7  app.vagrant          # GitLab App
10.8.0.31 gitaly01.vagrant     # GitLab Gitaly node
10.8.0.32 gitaly02.vagrant     
10.8.0.41 runner-10841.vagrant # GitLab gitlab-runner
10.8.0.42 runner-10842.vagrant 
```

## Scripts - Resets VMs

### scripts/reset-all-hosts.sh
By using the script `reset-all-hosts.sh` you will destroy all of the existing VMs, then create new ones with a bunch of 
snapshots for various GitLab versions.  This is intended as a **reset** of your current installation.

**Note:** This does use a lot of disk space, approx 110GB.  It also takes a while (hours...) so I recommend starting at the 
end of your day!  (as an aside the hosts will be in Australia time :-( - sorry, haven't figured this out yet)

```shell script
time bash scripts/reset-all-hosts.sh
```

At the end you will have the following snapshots (runner hosts will only do `.0` releases)

```text
# Example of snapshots, each VM has the snapshot taken, Runner hosts will only normally do `.0` releases
# ==> nfs:
...
# ==> app:
# 100-11.11.0-installed
# 120-11.11.8-updated
# 140-12.0.0-updated
# 160-12.0.9-updated
# 180-12.1.0-updated
# 200-12.2.0-updated
# 220-12.3.0-updated
# 240-12.4.0-updated
# 260-12.5.0-updated
# 280-12.6.0-updated
# 300-12.7.0-updated
# 320-12.8.0-updated
# ==> gitaly01:
...
# ==> gitaly02:
...
# ==> runner-10841:
# 100-11.11.0-installed
# 140-12.0.0-updated
# 180-12.1.0-updated
# 200-12.2.0-updated
# 220-12.3.0-updated
# 240-12.4.0-updated
# 260-12.5.0-updated
# 280-12.6.0-updated
# 300-12.7.0-updated
# 320-12.8.0-updated
# ==> runner-10842:
...
```

### scripts/show-gitlab-rb.sh - Show `gitlab.rb` settings used across VMs

```shell script
$ bash scripts/show-gitlab-rb.sh 

app
    external_url "http://app.vagrant"
    gitlab_rails['uploads_directory'] = '/gitlab-data/uploads'
    gitlab_rails['shared_path'] = '/gitlab-data/shared'
    user['home'] = '/gitlab-data/home'
    gitlab_ci['builds_directory'] = '/gitlab-data/builds'
    gitlab_rails['gitaly_token'] = '31lebkfkY8=bREPhtTWMiAwb'
    gitlab_rails['initial_shared_runners_registration_token'] = "38hFioub2dMqcz@niduu-sPv"
    git_data_dirs({
                      "default" => {
                          "path" => "/var/opt/gitlab/git-data",
                          "failure_count_threshold" => 10,
                          "failure_wait_time" => 30,
                          "failure_reset_time" => 1800,
                          "storage_timeout" => 30
                      },
                      "app-on-nfs" => {
                          "path" => "/gitlab-data/git-data",
                          "failure_count_threshold" => 10,
                          "failure_wait_time" => 30,
                          "failure_reset_time" => 1800,
                          "storage_timeout" => 30
                      },
                      "gitaly01-on-local" => {
                          "path" => "/srv/gitlab/gitaly01/repositories",
                          "gitaly_address" => "tcp://gitaly01:8075"
                      },
                      "gitaly01-on-nfs" => {
                          "path" => "/gitaly01-data/repositories",
                          "gitaly_address" => "tcp://gitaly01:8075"
                      },
                      "gitaly02-on-local" => {
                          "path" => "/srv/gitlab/gitaly02/repositories",
                          "gitaly_address" => "tcp://gitaly02:8075"
                      },
                      "gitaly02-on-nfs" => {
                          "path" => "/gitaly02-data/repositories",
                          "gitaly_address" => "tcp://gitaly02:8075"
                      },
                  })

gitaly01
    external_url "http://app.vagrant"
    postgresql['enable'] = false
    redis['enable'] = false
    nginx['enable'] = false
    prometheus['enable'] = false
    unicorn['enable'] = false
    sidekiq['enable'] = false
    gitlab_workhorse['enable'] = false
    gitlab_rails['rake_cache_clear'] = false
    gitlab_rails['auto_migrate'] = false
    gitlab_rails['internal_api_url'] = 'http://app.vagrant'
    gitaly['listen_addr'] = "0.0.0.0:8075"
    gitaly['auth_token'] = '31lebkfkY8=bREPhtTWMiAwb'
    gitaly['storage'] = [
        { 'name' => 'gitaly01-on-local', 'path' => '/srv/gitlab/gitaly01/repositories' },
        { 'name' => 'gitaly01-on-nfs', 'path' => '/gitaly01-data/repositories' },
    ]

gitaly02
    external_url "http://app.vagrant"
    postgresql['enable'] = false
    redis['enable'] = false
    nginx['enable'] = false
    prometheus['enable'] = false
    unicorn['enable'] = false
    sidekiq['enable'] = false
    gitlab_workhorse['enable'] = false
    gitlab_rails['rake_cache_clear'] = false
    gitlab_rails['auto_migrate'] = false
    gitlab_rails['internal_api_url'] = 'http://app.vagrant'
    gitaly['listen_addr'] = "0.0.0.0:8075"
    gitaly['auth_token'] = '31lebkfkY8=bREPhtTWMiAwb'
    gitaly['storage'] = [
        { 'name' => 'gitaly02-on-local', 'path' => '/srv/gitlab/gitaly02/repositories' },
        { 'name' => 'gitaly02-on-nfs', 'path' => '/gitaly02-data/repositories' },
    ]
```

### scripts/list-projects-storage-paths.sh - List projects across all VMs

An example of project being shared across the Gitaly hosts, in this case all Repository Storage options were enabled.

```shell script
$ bash scripts/list-projects-storage-paths.sh

app                                            
/var/opt/gitlab/git-data/repositories/root/p2.git   ### local
/var/opt/gitlab/git-data/repositories/root/p6.git
/var/opt/gitlab/git-data/repositories/root/p3.git
/gitlab-data/git-data/repositories/root/p12.git     ### NFS
/gitlab-data/git-data/repositories/root/p20.git
/gitlab-data/git-data/repositories/root/p19.git
/gitlab-data/git-data/repositories/root/p10.git

gitaly01
/srv/gitlab/gitaly01/repositories/root/p11.git      ### local
/srv/gitlab/gitaly01/repositories/root/p13.git
/gitaly01-data/repositories/root/p21.git            ### NFS

gitaly02
/srv/gitlab/gitaly02/repositories/root/p15.git
/gitaly02-data/repositories/root/p17.git
```
