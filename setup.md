# Setup

1. Install
* Virtualbox +6.0
* Vagrant +2.2.4
* Vagrant plugins
    * [vagrant-cachier](https://github.com/fgrehm/vagrant-cachier) - shares a common package cache among similar VM instances
    * [vagrant-hostsupdater](https://github.com/cogitatio/vagrant-hostsupdater) - adds an entry to your /etc/hosts file on the host system
    * [vagrant-vbguest](https://github.com/dotless-de/vagrant-vbguest) - automatically installs the host's VirtualBox Guest Additions on the guest system
    * [oscar](https://github.com/oscar-stack/oscar)

    
```shell script
vagrant plugin install vagrant-cachier
vagrant plugin install vagrant-hostsupdater
vagrant plugin install vagrant-vbguest
vagrant plugin install oscar
```

2. Create file `/etc/sudoers.d/vagrant_hostsupdater` with the contents below.  

This will stop the prompting for the root password to allow updates to `/etc/hosts`.

```shell script
sudo vi /etc/sudoers.d/vagrant_hostsupdater
```

* For OSx

Add text

```text
# Allow passwordless startup of Vagrant with vagrant-hostsupdater.
Cmnd_Alias VAGRANT_HOSTS_ADD = /bin/sh -c echo "*" >> /etc/hosts
Cmnd_Alias VAGRANT_HOSTS_REMOVE = /usr/bin/sed -i -e /*/ d /etc/hosts
%admin ALL=(root) NOPASSWD: VAGRANT_HOSTS_ADD, VAGRANT_HOSTS_REMOVE
```

* For Linux

```text
# Allow passwordless startup of Vagrant with vagrant-hostsupdater.
Cmnd_Alias VAGRANT_HOSTS_ADD = /bin/sh -c 'echo "*" >> /etc/hosts'
Cmnd_Alias VAGRANT_HOSTS_REMOVE = /usr/bin/env sed -i -e /*/ d /etc/hosts
%sudo ALL=(root) NOPASSWD: VAGRANT_HOSTS_ADD, VAGRANT_HOSTS_REMOVE 
```

3. Download box - bento/ubuntu-18.04 via 

```shell script
vagrant box add --provider virtualbox "bento/ubuntu-18.04"
```

4. For OSX - create `apt` directories for vagrant-cachier to use.

```shell script
mkdir -p ~/.vagrant.d/cache/bento/ubuntu-18.04/apt/partial
mkdir -p ~/.vagrant.d/cache/bento/ubuntu-18.04/apt_lists/partial
```