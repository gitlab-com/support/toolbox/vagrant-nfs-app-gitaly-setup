git_data_dirs({
                  "default" => {
                      "path" => "/var/opt/gitlab/git-data",
                      "failure_count_threshold" => 10,
                      "failure_wait_time" => 30,
                      "failure_reset_time" => 1800,
                      "storage_timeout" => 30
                  },
                  "app-on-nfs" => {
                      "path" => "/gitlab-data/git-data",
                      "failure_count_threshold" => 10,
                      "failure_wait_time" => 30,
                      "failure_reset_time" => 1800,
                      "storage_timeout" => 30
                  },
                  "gitaly01-on-local" => {
                      "path" => "/srv/gitlab/gitaly01/repositories",
                      "gitaly_address" => "tcp://gitaly01:8075"
                  },
                  "gitaly01-on-nfs" => {
                      "path" => "/gitaly01-data/repositories",
                      "gitaly_address" => "tcp://gitaly01:8075"
                  },
                  "gitaly02-on-local" => {
                      "path" => "/srv/gitlab/gitaly02/repositories",
                      "gitaly_address" => "tcp://gitaly02:8075"
                  },
                  "gitaly02-on-nfs" => {
                      "path" => "/gitaly02-data/repositories",
                      "gitaly_address" => "tcp://gitaly02:8075"
                  },
              })
