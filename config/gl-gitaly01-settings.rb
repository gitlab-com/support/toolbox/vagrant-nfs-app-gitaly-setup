
postgresql['enable'] = false
redis['enable'] = false
nginx['enable'] = false
prometheus['enable'] = false
unicorn['enable'] = false
sidekiq['enable'] = false
gitlab_workhorse['enable'] = false
gitlab_rails['rake_cache_clear'] = false
gitlab_rails['auto_migrate'] = false

gitlab_rails['internal_api_url'] = 'http://app.vagrant'

gitaly['listen_addr'] = "0.0.0.0:8075"
gitaly['auth_token'] = '31lebkfkY8=bREPhtTWMiAwb'

gitaly['storage'] = [
    { 'name' => 'gitaly01-on-local', 'path' => '/srv/gitlab/gitaly01/repositories' },
    { 'name' => 'gitaly01-on-nfs', 'path' => '/gitaly01-data/repositories' },
]

