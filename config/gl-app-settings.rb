
gitlab_rails['gitaly_token'] = '31lebkfkY8=bREPhtTWMiAwb'

#### Change the initial default admin password and shared runner registration tokens.
####! **Only applicable on initial setup, changing these settings after database
####!   is created and seeded won't yield any change.**
# gitlab_rails['initial_root_password'] = "password"
gitlab_rails['initial_shared_runners_registration_token'] = "38hFioub2dMqcz@niduu-sPv"

#### Set path to an initial license to be used while bootstrapping GitLab.
####! **Only applicable on initial setup, future license updations need to be done via UI.
####! Updating the file specified in this path won't yield any change after the first reconfigure run.
# gitlab_rails['initial_license_file'] = '/etc/gitlab/company.gitlab-license'

#TODO
#Disable prometheus, kubernetes, exporters
