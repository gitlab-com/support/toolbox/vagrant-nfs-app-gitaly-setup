#!/usr/bin/env bash
#

echo "app"
vagrant ssh -c 'sudo find /var/opt/gitlab/git-data/repositories/ -type d -name "*\.git" >/tmp/project.lis ; sudo find /gitlab-data/git-data/repositories/ -type d -name "*\.git" >>/tmp/project.lis ; grep -Eiv wiki /tmp/project.lis' app


echo
echo "gitaly01"
vagrant ssh -c 'sudo find /srv/gitlab/gitaly01/repositories/ -type d -name "*\.git" >/tmp/project.lis ; sudo find /gitaly01-data/repositories/ -type d -name "*\.git" >>/tmp/project.lis ; grep -Eiv wiki /tmp/project.lis' gitaly01

echo
echo "gitaly02"
vagrant ssh -c 'sudo find /srv/gitlab/gitaly02/repositories/ -type d -name "*\.git" >/tmp/project.lis ; sudo find /gitaly02-data/repositories/ -type d -name "*\.git" >>/tmp/project.lis ; grep -Eiv wiki /tmp/project.lis' gitaly02
