#!/usr/bin/env bash
#

echo
for myh in nfs app gitaly01 gitaly02 runner-10841 runner-10842
do
  echo "VM: $myh"
  vagrant ssh -c 'timedatectl' $myh
  echo
done

