#!/usr/bin/env bash
#
# This script is DESTRUCTIVE
# It will delete all existing VMs,then install 11.11.5 via `git tag v11.11.5` and upgrade through
# GITLAB_VERSIONS taking snapshots along the way.
#
# It's meant as a reset, refresh your setup
# Note: it does use diskspace!  e.g the exaple used 110GB
#  10:33 $ du -sh /Volumes/Transcend001/VirtualBox\ VMs/vagrant-nfs-app-gitaly-setup_*
#  32G	/Volumes/Transcend001/VirtualBox VMs/vagrant-nfs-app-gitaly-setup_app_1582627794560_54589
#  31G	/Volumes/Transcend001/VirtualBox VMs/vagrant-nfs-app-gitaly-setup_gitaly01_1582628457936_49858
#  31G	/Volumes/Transcend001/VirtualBox VMs/vagrant-nfs-app-gitaly-setup_gitaly02_1582629011379_3690
# 4.5G	/Volumes/Transcend001/VirtualBox VMs/vagrant-nfs-app-gitaly-setup_nfs_1582627516508_20651
# 5.4G	/Volumes/Transcend001/VirtualBox VMs/vagrant-nfs-app-gitaly-setup_runner-10841_1582629490218_59975
# 5.4G	/Volumes/Transcend001/VirtualBox VMs/vagrant-nfs-app-gitaly-setup_runner-10842_1582629896321_25168
# Example of snapshots, each VM has the snapshot taken, Runner hosts will only normally do `.0` releases
# ==> nfs:
# ==> app:
# 100-11.11.0-installed
# 120-11.11.8-updated
# 140-12.0.0-updated
# 160-12.0.9-updated
# 180-12.1.0-updated
# 200-12.2.0-updated
# 220-12.3.0-updated
# 240-12.4.0-updated
# 260-12.5.0-updated
# 280-12.6.0-updated
# 300-12.7.0-updated
# 320-12.8.0-updated
# ==> gitaly01:
# ==> gitaly02:
# ==> runner-10841:
# 100-11.11.0-installed
# 140-12.0.0-updated
# 180-12.1.0-updated
# 200-12.2.0-updated
# 220-12.3.0-updated
# 240-12.4.0-updated
# 260-12.5.0-updated
# 280-12.6.0-updated
# 300-12.7.0-updated
# 320-12.8.0-updated
# ==> runner-10842:
#

if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
  # Bash 4.4, Zsh
  set -euo pipefail
else
  # Bash 4.3 and older chokes on empty arrays with set -u.
  set -eo pipefail
fi
shopt -s nullglob globstar

BASE_DIR=$(realpath "$(dirname "${BASH_SOURCE[0]%/*}")")  ## setting to dir containing Vagrantfile

SNAPSHOT_COUNT_INITIAL=100
SNAPSHOT_INCREMENT=20
GITLAB_INITIAL_VERSION="11.11.0" ### matches with git tags for this project
GITLAB_VERSIONS="11.11.8 12.0.0 12.0.9 12.1.0 12.2.0 12.3.0 12.4.0 12.5.0 12.6.0 12.7.0 12.8.0"

# using git tag, to ensure initial install work normally
CURRENT_BRANCH=$(git branch --show-current)
git stash

if [[ "$GITLAB_INITIAL_VERSION" == "11.11.0" ]]; then
  git checkout "v11.11.5" ### matches with git tags for this project, started with 11.11.5

else
  git checkout "v${GITLAB_INITIAL_VERSION}"
fi

# now do the initial install...
SNAPSHOT_COUNT=$SNAPSHOT_COUNT_INITIAL
for myhost in nfs app gitaly01 gitaly02 runner-10841 runner-10842; do
  cd "${BASE_DIR}"
  echo "Host: ${myhost}"
  vagrant destroy -f $myhost
  vagrant up $myhost
  vagrant halt $myhost
  vagrant snapshot save $myhost "${SNAPSHOT_COUNT}-${GITLAB_INITIAL_VERSION}-installed"
  vagrant up $myhost
done

git checkout $CURRENT_BRANCH
git stash pop

# now do the upgrading...
cd "${BASE_DIR}"
for myversion in $GITLAB_VERSIONS; do
  echo
  echo "Version: $myversion"
  echo
  SNAPSHOT_COUNT=$(($SNAPSHOT_COUNT + $SNAPSHOT_INCREMENT))
  for myhost in gitaly01 gitaly02 app runner-10841 runner-10842; do
    if [[ "${myversion##*.}" != "0" && ("$myhost" == "runner-10841" || "$myhost" == "runner-10842") ]]; then
      continue
    fi
    echo "Host: ${myhost}"
    vagrant up $myhost
    vagrant ssh $myhost -c "sleep 60"
    vagrant ssh $myhost -c "sudo apt-get -qq update"
    if [[ "$myhost" == "runner-10841" || "$myhost" == "runner-10842" ]]; then
      vagrant ssh $myhost -c "sudo apt-get -qq install -y gitlab-runner=${myversion}"
    else
      vagrant ssh $myhost -c "sudo apt-get -qq install -y gitlab-ee=${myversion}-ee.0"
    fi
    vagrant halt $myhost
    vagrant snapshot save $myhost "${SNAPSHOT_COUNT}-${myversion}-updated"
    vagrant up $myhost
  done
  # take a snapshot of nfs to allow rollback all to a specific install
  vagrant snapshot save nfs "${SNAPSHOT_COUNT}-${myversion}-updated"
done
