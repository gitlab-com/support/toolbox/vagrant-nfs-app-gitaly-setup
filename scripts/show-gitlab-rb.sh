#!/usr/bin/env bash
#

echo
for myh in app gitaly01 gitaly02
do
  echo "VM: $myh"
  vagrant ssh -c 'sudo grep -Eiv "^$|^#" /etc/gitlab/gitlab.rb' $myh
  echo
done