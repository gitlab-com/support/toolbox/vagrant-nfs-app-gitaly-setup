#!/bin/bash
#
# https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nfs-mount-on-ubuntu-16-04
#

sudo apt-get -qq install nfs-kernel-server >/dev/null

#
# Restore doesn't work when certain directories are mounted on NFS
# https://gitlab.com/gitlab-org/gitlab-ce/issues/19309
# workaround

# craete git user
sudo groupadd -g 997 git
sudo useradd -u 997 -g 997 git

# test export
sudo mkdir -p /var/nfs/general
sudo chown nobody:nogroup /var/nfs/general

# app storage
sudo mkdir -p /var/nfs/gitlab-data

sudo mkdir     /var/nfs/gitlab-data/git-data
sudo mkdir     /var/nfs/gitlab-data/.ssh
sudo mkdir     /var/nfs/gitlab-data/uploads
sudo mkdir     /var/nfs/gitlab-data/shared
sudo mkdir     /var/nfs/gitlab-data/builds
sudo chown git /var/nfs/gitlab-data

# gitaly01 storage
sudo mkdir -p  /var/nfs/gitaly01-data
sudo mkdir -p  /var/nfs/gitaly01-data/repositories
sudo chown git /var/nfs/gitaly01-data
sudo chown git /var/nfs/gitaly01-data/repositories
sudo chmod go-rwx /var/nfs/gitaly01-data
sudo chmod o-rwx,g+rwx,g+s /var/nfs/gitaly01-data/repositories/

# gitaly02 storage
sudo mkdir -p  /var/nfs/gitaly02-data
sudo mkdir -p  /var/nfs/gitaly02-data/repositories
sudo chown git /var/nfs/gitaly02-data
sudo chown git /var/nfs/gitaly02-data/repositories
sudo chmod go-rwx /var/nfs/gitaly02-data
sudo chmod o-rwx,g+rwx,g+s /var/nfs/gitaly02-data/repositories/

# setup exports
cat <<-EOF >/etc/exports
/var/nfs/general        10.8.0.0/24(rw,sync,no_subtree_check)
/var/nfs/gitlab-data    10.8.0.0/24(rw,no_root_squash,sync,no_subtree_check)
/var/nfs/gitaly01-data  10.8.0.0/24(rw,no_root_squash,sync,no_subtree_check)
/var/nfs/gitaly02-data  10.8.0.0/24(rw,no_root_squash,sync,no_subtree_check)
EOF

sudo systemctl restart nfs-kernel-server