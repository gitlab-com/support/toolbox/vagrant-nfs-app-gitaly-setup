#!/bin/bash
#
# $1 NFS type - settings['setup']['nfs']

# Set NFS path redirection if needed
if [[ $1 == "use-single-mountpoint" ]]; then
  sudo cat /vagrant/config/gl-app-nfs-settings.rb >> /etc/gitlab/gitlab.rb
elif [[ $1 == "use-bind-mounts" ]]; then
  : # do nothing, configured in nfs-client.sh
fi