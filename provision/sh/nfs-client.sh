#!/bin/bash
#
# https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nfs-mount-on-ubuntu-16-04
#
# $1 settings['setup']['nfs']
# $2 hostname

NFS_MOUNT_TYPE="$1"
HOSTNAME="$2"

sudo apt-get -qq install nfs-common >/dev/null


if [[ $HOSTNAME == "app" ]]
then
  cat <<-EOF >>/etc/fstab

nfs:/var/nfs/general      /nfs/general   nfs4 auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
nfs:/var/nfs/gitlab-data  /gitlab-data   nfs4 defaults,soft,rsize=1048576,wsize=1048576,noatime,nofail,lookupcache=positive 0 2
EOF

  if [[ $NFS_MOUNT_TYPE == "use-single-mountpoint" ]]; then
    : # do nothing, gitlab.rb is configured in configs
  elif [[ $NFS_MOUNT_TYPE == "use-bind-mounts" ]]; then
    cat <<-EOF >>/etc/fstab

# Using https://docs.gitlab.com/ee/administration/high_availability/nfs.html#bind-mounts
/gitlab-data/git-data /var/opt/gitlab/git-data none bind 0 0
/gitlab-data/.ssh /var/opt/gitlab/.ssh none bind 0 0
/gitlab-data/uploads /var/opt/gitlab/gitlab-rails/uploads none bind 0 0
/gitlab-data/shared /var/opt/gitlab/gitlab-rails/shared none bind 0 0
/gitlab-data/builds /var/opt/gitlab/gitlab-ci/builds none bind 0 0
EOF
  fi

  sudo mkdir -p /nfs/general
  sudo mkdir -p /gitlab-data

  sudo mkdir -p /var/opt/gitlab/git-data
  sudo mkdir -p /var/opt/gitlab/.ssh
  sudo mkdir -p /var/opt/gitlab/gitlab-rails/uploads
  sudo mkdir -p /var/opt/gitlab/gitlab-rails/shared
  sudo mkdir -p /var/opt/gitlab/gitlab-ci/builds
fi

if [[ $HOSTNAME = "gitaly01" || $HOSTNAME = "gitaly02" ]]
then
  cat <<-EOF >>/etc/fstab

nfs:/var/nfs/${HOSTNAME}-data  /${HOSTNAME}-data   nfs4 defaults,soft,rsize=1048576,wsize=1048576,noatime,nofail,lookupcache=positive 0 2
EOF

  sudo mkdir -p /${HOSTNAME}-data
fi

sudo mount -a


#
#
# Redhat uses nobootwait - nfs:/var/nfs/gitlab /var/nfs/gitlab nfs defaults,soft,rsize=1048576,wsize=1048576,noatime,nobootwait,lookupcache=positive 0 2
# Ubuntu uses nofail     - nfs:/var/nfs/gitlab /var/nfs/gitlab nfs defaults,soft,rsize=1048576,wsize=1048576,noatime,nofail,lookupcache=positive 0 2
#
#