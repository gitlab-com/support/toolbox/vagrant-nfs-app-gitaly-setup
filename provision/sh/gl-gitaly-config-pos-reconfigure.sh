#!/bin/bash
#
# $1 hostname

HOSTNAME=$1

# "path" => "/srv/gitlab/...",

##TODO check our docs on directory permissions, this may be missing from the standalone setup.
##TODO replace with numbers - this is easier to read!
chown git:git "/srv/gitlab/${1}/repositories"
chmod o-rwx   "/srv/gitlab/${1}/repositories"
chmod g+ws    "/srv/gitlab/${1}/repositories"

chown git     "/srv/gitlab/${1}"
chmod go-rwx  "/srv/gitlab/${1}"


if [[ $HOSTNAME == "gitaly02" ]]
then
  chown git:git /gitaly02-data/repositories
  chmod o-rwx   /gitaly02-data/repositories
  chmod g+ws    /gitaly02-data/repositories

#  chown git     "/srv/gitlab/${1}"
#  chmod go-rwx  "/srv/gitlab/${1}"
fi
