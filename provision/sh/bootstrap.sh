#!/bin/bash
# $1 timezone
#

OS_TIMEZONE="${1}"

# to stop errors re. stdin on install
# https://serverfault.com/questions/500764/dpkg-reconfigure-unable-to-re-open-stdin-no-file-or-directory
sudo ex +"%s@DPkg@//DPkg" -cwq /etc/apt/apt.conf.d/70debconf
sudo dpkg-reconfigure debconf -f noninteractive -p critical
sudo apt-get -qq update

# timesync
# https://www.digitalocean.com/community/tutorials/how-to-set-up-time-synchronization-on-ubuntu-18-04
sudo timedatectl set-timezone "${OS_TIMEZONE}"

sudo timedatectl set-ntp true
sudo systemctl restart systemd-timesyncd.service
