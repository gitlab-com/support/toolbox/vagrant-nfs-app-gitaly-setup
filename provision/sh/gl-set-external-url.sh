#!/bin/bash
#
# $1 external_url - nodename?.vm.hostname

# Set external_url
sudo sed -i "s,external_url.*,external_url "\"http://${1}\"",g" /etc/gitlab/gitlab.rb