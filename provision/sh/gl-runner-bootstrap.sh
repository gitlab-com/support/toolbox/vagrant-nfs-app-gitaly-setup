#!/bin/bash
#
# $1 gitlab-version

GITLAB_VERSION="$1"

echo "Running sudo apt-get -qq update; sudo apt-get -qq install -y curl openssh-server ca-certificates"
sudo apt-get -qq update
sudo apt-get -qq install curl openssh-server ca-certificates >/dev/null

echo "Running curl -s --retry 5 -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash"
curl -s --retry 5 -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash


#
#TODO we are installing gitlab-version latest, and gitlab-runner-version latest - we need
# to work out the version for DEB based systems
#   apt-cache madison gitlab-runner
# will give a listing of available version
#
if [[ "${GITLAB_VERSION}" == "" ]]
then
  echo "Running sudo apt-get -qq install gitlab-runner"
  sudo apt-get -qq install gitlab-runner  >/dev/null
else
  echo "Running sudo apt-get -qq install gitlab-runner=${1}"
  sudo apt-get -qq install "gitlab-runner=${1}"  >/dev/null
fi

