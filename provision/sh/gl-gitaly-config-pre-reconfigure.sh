#!/bin/bash
#
# $1 hostname

# "path" => "/srv/gitlab/...",

mkdir -p      "/srv/gitlab/${1}/repositories"

sudo cat /vagrant/config/gl-${1}-settings.rb >> /etc/gitlab/gitlab.rb