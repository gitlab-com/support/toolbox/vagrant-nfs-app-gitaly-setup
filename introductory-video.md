# Introductory Video

## Intoduction
Notes on what we want to highlight as part of the video presentation.

## Pre-reqs
Have a freshly provisioned environment - good to go.  This will save waiting for the provisioning time and gives us the ability to demonstrate the necessary GitLab Admin tasks in the UI.

To reuse an existing setup

```shell script
vagrant destroy -f
time vagrant up     ### we want to highlight the time taken
```

or clone into a new directory.

If you already have running VMs you will probably need to halt these due to memory requirements.

## Script

### Intro (AlexS)
* Show both projects on GitLab.com
* Highlight `toolbox` location.
* Inform - why, what and how of the projects
    * why
        * workshop/sandpit environments for different GitLab versions used in ticket support or bootcamps
        * allowing testing/confirmation of operational tasks - backup/restore
    * what
        * Virtualbox, Vagrant
        * VMs
        * vagrant snapshot
    * how
        * setup is wrapping documentation commands around shell scripts
        * reducing complexity by assuming a pre-defined setup

### Gitaly (Julianne)
* Run through **Setup** commands, this should be fast as you will have already performed the actions already so should be fine to run live
* Inform what the commands are doing and how they help
  * hostupdater - add to local /etc/hosts file so you don't need to use IP addresses (cat your local `/etc/hosts` file)
  * cachier - caches GitLab package to stop multiple downloads
    
### Geo (assuming on Linux) (James)
* Run through **Setup** where it varies     

### Gitaly (Julianne)
* Run through **Getting Started**
* Show results from previous `time vagrant up` command.  
* Show use of scripts directory
   
### Geo (James)   
* Run through **Getting Started**
* Show results from previous `time vagrant up` command.  
* Show use of scripts directory
* Show use of vagrant snapshot
    For speed
    ```shell script
    vagrant halt
    vagrant snapshot save 100-installed-x.y.z
    ```     
  