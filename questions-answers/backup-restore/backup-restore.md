# Question - Backup / Restore

## Setup - Settings
1. Disable Ci/CD Auto DevOPS in the Admin settings (to keep clean screenshots) [Admin CI/CD](http://app.vagrant/admin/application_settings/ci_cd)
1. Disable `Use hashed storage paths` to allow easier visibility of project names [Admin Repository](http://app.vagrant/admin/application_settings/repository) > Repository Storage
1. Activate additional `Storage paths` [Admin Repository](http://app.vagrant/admin/application_settings/repository) > Repository Storage

## Question - Do the projects on the Gitaly nodes get backed up?
### Backup
#### Ensure multiple Gitaly nodes are configured

![Gitalt nodes](gitaly-nodes-admin-overview.png)

#### Ensure [Admin Repository](http://app.vagrant/admin/application_settings/repository) > Repository storage is **Checked** for multiple storage paths

![Repository Storage](gitlab-settings-repository-storage.png)

#### Create 5 Projects 

1. Project 100 - Internal and **Initialize repository with a README**
1. Project 101 - Internal and **Initialize repository with a README**
1. Project 102 - Internal
1. Mirror [TortoiseGit](https://gitlab.com/tortoisegit/tortoisegit) by git URL https://gitlab.com/tortoisegit/tortoisegit.git
1. Mirror [freedesktop-sdk](https://gitlab.com/freedesktop-sdk/freedesktop-sdk) by git URL https://gitlab.com/freedesktop-sdk/freedesktop-sdk.git

#### Confirm projects stored on file system
Confirm the location of projects on the filesystem.  We enabled all three in the settings above, this means that **New Projects** will assigned a Gitaly server randomly.

```shell script
$ vagrant ssh -c 'sudo find /gitlab-data/git-data/repositories/ -type d -name "*\.git" | grep -Eiv wiki' app
/gitlab-data/git-data/repositories/root/freedesktop-sdk.git

$ vagrant ssh -c 'sudo find /srv/gitlab/gitaly01/repositories/ -type d -name "*\.git" | grep -Eiv wiki' gitaly01
/srv/gitlab/gitaly01/repositories/root/project-102.git
/srv/gitlab/gitaly01/repositories/root/project-100.git

$ vagrant ssh -c 'sudo find /srv/gitlab/gitaly02/repositories/ -type d -name "*\.git" | grep -Eiv wiki' gital
y02
/srv/gitlab/gitaly02/repositories/root/tortoisegit.git
/srv/gitlab/gitaly02/repositories/root/project-101.git
```

> This shows the **random** order with the app host getting the `freedesktop-sdk` project.

#### Run backup

Running rake task.
```shell script
$ vagrant ssh app
$ sudo gitlab-rake gitlab:backup:create
2019-09-02 02:33:50 +0000 -- Dumping database ...
2019-09-02 02:33:52 +0000 -- Dumping repositories ...
 * root/project-100 ... [DONE]
 * root/project-101 ... [DONE]
 * root/project-102 ... [SKIPPED]     ### Empty project!
 * root/tortoisegit ... [DONE]
 * root/freedesktop-sdk ... [DONE]
...
Creating backup archive: 1567391649_2019_09_02_12.2.3-ee_gitlab_backup.tar ... done
```

Confirm contents of `tar` file

```shell script
$ sudo tar tvf /var/opt/gitlab/backups/1567391649_2019_09_02_12.2.3-ee_gitlab_backup.tar
drwx------ git/git           0 2019-09-02 02:33 repositories/
drwxr-xr-x git/git           0 2019-09-02 02:34 repositories/root/
-rw-r--r-- git/git         350 2019-09-02 02:33 repositories/root/project-100.bundle
drwxr-xr-x git/git           0 2019-09-02 02:34 repositories/root/freedesktop-sdk/
-rw-r--r-- git/git     8186040 2019-09-02 02:34 repositories/root/freedesktop-sdk.bundle
drwxr-xr-x git/git           0 2019-09-02 02:33 repositories/root/project-100/
-rw-r--r-- git/git         352 2019-09-02 02:33 repositories/root/project-101.bundle
-rw-r--r-- git/git   132946323 2019-09-02 02:34 repositories/root/tortoisegit.bundle
drwxr-xr-x git/git           0 2019-09-02 02:33 repositories/root/project-101/
drwxr-xr-x git/git           0 2019-09-02 02:34 repositories/root/tortoisegit/
drwxr-xr-x git/git           0 2019-09-02 02:33 db/
-rw------- git/git      117037 2019-09-02 02:33 db/database.sql.gz
-rw------- git/git         151 2019-09-02 02:34 uploads.tar.gz
-rw------- git/git         151 2019-09-02 02:34 builds.tar.gz
-rw------- git/git         151 2019-09-02 02:34 artifacts.tar.gz
-rw------- git/git         154 2019-09-02 02:34 pages.tar.gz
-rw------- git/git         151 2019-09-02 02:34 lfs.tar.gz
-rw-r--r-- git/git         193 2019-09-02 02:34 backup_information.yml
```

**All** of the projects are included in the backup.

#### Delete **Project 101**

List projects
```shell script
$ scripts/list-projects-storage-paths.sh
app host
/gitlab-data/git-data/repositories/root/freedesktop-sdk.git

gitaly01 host
/srv/gitlab/gitaly01/repositories/root/project-102.git
/srv/gitlab/gitaly01/repositories/root/project-100.git

gitaly02 host
/srv/gitlab/gitaly02/repositories/root/tortoisegit.git
```

**Project 101** is no longer listed.

#### Restore - Will restore put the projects back to the original nodes?
Run restore

```shell script
$ sudo gitlab-rake gitlab:backup:restore
Unpacking backup ... done
Before restoring the database, we will remove all existing
tables to avoid future upgrade problems. Be aware that if you have
custom tables in the GitLab database these tables and all data will be
removed.

Do you want to continue (yes/no)? yes
Removing all tables. Press `Ctrl-C` within 5 seconds to abort
2019-09-02 02:55:00 +0000 -- Cleaning the database ...
2019-09-02 02:55:02 +0000 -- done
2019-09-02 02:55:02 +0000 -- Restoring database ...
...
[DONE]
2019-09-02 02:55:22 +0000 -- done
2019-09-02 02:55:22 +0000 -- Restoring repositories ...
 * root/project-100 ... [DONE]
 * root/project-101 ... [DONE]     ### restored from backup
 * root/project-102 ... [DONE]
 * root/tortoisegit ... [DONE]
 * root/freedesktop-sdk ... [DONE]
2019-09-02 02:55:47 +0000 -- done
...
Warning: Your gitlab.rb and gitlab-secrets.json files contain sensitive data
and are not included in this backup. You will need to restore these files manually.
Restore task is done.
```

Confirm the location of projects on the filesystem

```shell script
$ scripts/list-projects-storage-paths.sh
app host
/gitlab-data/git-data/repositories/+gitaly/tmp/default-repositories.old.1567392923.981648814/root/freedesktop-sdk.git
/gitlab-data/git-data/repositories/root/freedesktop-sdk.git

gitaly01 host
/srv/gitlab/gitaly01/repositories/+gitaly/tmp/storage1-repositories.old.1567392921.173048296/root/project-102.git
/srv/gitlab/gitaly01/repositories/+gitaly/tmp/storage1-repositories.old.1567392921.173048296/root/project-100.git
/srv/gitlab/gitaly01/repositories/root/project-102.git
/srv/gitlab/gitaly01/repositories/root/project-100.git

gitaly02 host
/srv/gitlab/gitaly02/repositories/+gitaly/tmp/storage2-repositories.old.1567392922.598806661/root/tortoisegit.git
/srv/gitlab/gitaly02/repositories/root/tortoisegit.git
/srv/gitlab/gitaly02/repositories/root/project-101.git    ### Restored to the same location
```

**Project 101** has been restored to the same location.

These directories are created by the `restore` rake task, they contain the projects before the new projects are extracted.

```shell script
app host
/gitlab-data/git-data/repositories/+gitaly/tmp/default-repositories.old.1567392923.981648814/root/freedesktop-sdk.git

gitaly01 host
/srv/gitlab/gitaly01/repositories/+gitaly/tmp/storage1-repositories.old.1567392921.173048296/root/project-102.git
/srv/gitlab/gitaly01/repositories/+gitaly/tmp/storage1-repositories.old.1567392921.173048296/root/project-100.git

gitaly02 host
/srv/gitlab/gitaly02/repositories/+gitaly/tmp/storage2-repositories.old.1567392922.598806661/root/tortoisegit.git
```
