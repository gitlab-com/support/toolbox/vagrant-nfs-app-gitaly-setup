# Question - Update GitLab Software

## What order should GitLab be updated in a multi-host setup?

In this setup we have

* nfs      - NFS Storage (only used by app host)
* app      - GitLab Application host
* gitaly01 - GitLab Standalone Gitaly host
* gitaly02 - GitLab Standalone Gitaly host

The order is documented at [Multi-node / HA deployment](https://docs.gitlab.com/omnibus/update/#multi-node--ha-deployment) where you start with a nominated GitLab Application host then rollout from there.

In this setup our order will be `app > gitaly01 > gitaly02`

## Update app - GitLab Application host

Update to the latest via the OS package manager or to a specific version as needed.  To update to a specific version go to [GitLab EE Packages](https://packages.gitlab.com/gitlab/gitlab-ee) for instructions.

As this setup is not HA and using a single application node we can update as a standalone GitLab Omnibus installation.

Currently we have **11.11.1** installed

![GitLab Help](help-11-11-5.png)

![Gitaly Servers Overview](admin-gitaly-hosts-before-update.png)

Version 11.11.x was the last release of GitLab before going to 12.x.x.  This means that our upgrade path is

1. upgrade app host to the latest 11.11.x version, see [GitLab EE Packages](https://packages.gitlab.com/gitlab/gitlab-ee) for this.
1. upgrade Gitaly hosts (gitaly01, gitaly02) to latest 11.11.x
1. upgrade app to latest 12.x.x, currently 12.2.4
1. upgrade Gitaly hosts (gitaly01, gitaly02) to latest 12.x.x

Review these for more detail and specifc version upgrade steps 

* [Updating GitLab installed with the Omnibus GitLab package
](https://docs.gitlab.com/omnibus/update/)
* [Upgrade recommendations](https://docs.gitlab.com/ee/policy/maintenance.html#upgrade-recommendations)

When updating app host to **11.11.5** via `sudo apt-get install gitlab-ee=11.11.5-ee.0` only a backup of the database is taken with all other components being skipped.

Here is a snippet of logs

```shell script
app$ sudo apt-get install gitlab-ee=11.11.5-ee.0
...
Preparing to unpack .../gitlab-ee_11.11.5-ee.0_amd64.deb ...
gitlab preinstall: Automatically backing up only the GitLab SQL database (excluding everything else!)
2019-09-03 08:40:32 +0000 -- Dumping database ...
Dumping PostgreSQL database gitlabhq_production ... [DONE]
2019-09-03 08:40:33 +0000 -- done
2019-09-03 08:40:33 +0000 -- Dumping repositories ...
2019-09-03 08:40:33 +0000 -- [SKIPPED]
2019-09-03 08:40:33 +0000 -- Dumping uploads ...
2019-09-03 08:40:33 +0000 -- [SKIPPED]
... 
Upgrade complete! If your GitLab server is misbehaving try running
  sudo gitlab-ctl restart
before anything else.
```

Now when we go to Admin > Overview > Gitaly Servers we see that only `default` is green.  This is due to the Gitaly standalone hosts being on a different version of the software.

![Gitaly Servers Overview (app host updated)](app-host-updated.png)

## Update gitaly01 - GitLab Standalone Gitaly host

Update the GitLab software to the same version as the app host.

Also done with `sudo apt-get install gitlab-ee=11.11.5-ee.0` as a Gitaly standalone host uses the same Omnibus package with only Gitaly being enabled.

```shell script
gitaly01:~$ sudo apt-get install gitlab-ee=11.11.5-ee.0
...
Preparing to unpack .../gitlab-ee_11.11.5-ee.0_amd64.deb ...
gitlab preinstall:
gitlab preinstall: This node does not appear to be running a database
gitlab preinstall: Skipping version check, if you think this is an error exit now
gitlab preinstall:
Unpacking gitlab-ee (11.11.5-ee.0) over (11.11.1-ee.0) ...
```

This time there was no backup of the database done as it's not a running component on this host.

Again, reviewing Admin > Overview > Gitaly Servers we now have two Storage paths showing as green.

![Gitaly Servers Overview (gitaly01 host updated)](gitaly01-host-updated.png)
 

## Repeat for gitaly02

![Gitaly Servers Overview (gitaly02 host updated)](gitaly02-host-updated.png)

## Repeat for upgrading to **12.2.4**

Start again with updating the app host to **12.2.4**, then follow up with the Gitaly hosts.

